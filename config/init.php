<?php

if (!defined('ROOT')) define('ROOT', dirname(__DIR__));
//if (!defined('WWW')) define('WWW', public_path());
//if (!defined('APP')) define('APP', app_path());
//if (!defined('CORE')) define('CORE', resource_path());
//if (!defined('LIBS')) define('LIBS', resource_path('libs'));
if (!defined('CACHE')) define('CACHE', ROOT . '/tmp/cache');
if (!defined('CONF')) define('CONF', ROOT . '/config');
if (!defined('LAYOUT')) define('LAYOUT', 'app.blade.php');
//if (!defined('GALLERY')) define('GALLERY', public_path('uploads/gallery'));
//if (!defined('IMG')) define('IMG', public_path('uploads/single'));

//TODO: allowed_hosts? 4. 9:20
$allowed_hosts = "http://localhost/index.php";
//$allowed_hosts = "https://larka.herokuapp.com/index.php";
$app_path = preg_replace("#[^/]+$#", '', $allowed_hosts);
$app_path = preg_replace("/public/", '', $app_path);

if (!defined('PATH')) define('PATH', $app_path);
if (!defined('ADMIN')) define('ADMIN', PATH . 'admin/index'); //TODO: check trailing slash

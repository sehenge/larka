<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route; //TODO: check usage later

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/index', 'Blog\User\MainController@index')->name('user');
Route::get('/admin', function() {
    return redirect('/admin/index');
});

/** Admin panel */
Route::group(['middleware' => ['status', 'auth']], function () {
    $groupData = [
        'namespace' => 'Blog\Admin',
        'prefix'    => 'admin',
    ];

    Route::group($groupData, function () {
        Route::resource('index', 'MainController')
            ->names('blog.admin.index');
    });
});

/** User panel */
Route::group(['middleware' => ['status', 'auth']], function () {
    $groupData = [
        'namespace' => 'Blog\User',
        'prefix'    => 'user',
    ];

    Route::group($groupData, function () {
        Route::resource('index', 'MainController')
            ->names('blog.user.index');
    });
});

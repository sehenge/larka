<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function testSelectTest()
    {
        $results = DB::select('select * from users where id = 3');
        $await = $results[0]->name;
        $this->assertTrue($await == 'sasha');
        $results = DB::select('select * from users where id = 14');
        $await = $results[0]->name;
        $this->assertTrue($await == 'Alex Moon');
    }
}
